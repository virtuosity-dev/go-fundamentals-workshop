package main

import (
	"fmt"
	// 1
	"gitlab.com/virtuosity-dev/go-fundamentals-workshop/09_OOP/02_encapsulation/drinks"
)

func main() {
	// 2
	var drinksSlice	[]drinks.Drinks
	drinksSlice = append(drinksSlice, drinks.Coffee{Base: "Americano"})
	drinksSlice = append(drinksSlice, drinks.Juice{Fruit: "Orange"})
	drinksSlice = append(drinksSlice, drinks.IceCoffee{
		Coffee: drinks.Coffee{Base: "Latte"},
		IceLevel: "half",
	})

	// 3 cannot access because const "coffeeShop" is unexported
	// drinks.coffeeShop
	drinks.Greet()
	for i, d := range drinksSlice {
		fmt.Print(i+1, ") ")
		d.MakeDrinks()
	}
}
