package drinks

import "fmt"

// 1a
type Drinks interface {
	// 1c
	MakeDrinks()
}
type Coffee struct {
	// 1b
	Base string
}
type Juice struct {
	// 1b
	Fruit string
}
type IceCoffee struct {
	// 1b
	Coffee
	IceLevel string
}

func (c Coffee) MakeDrinks() {
	fmt.Println("Fills cup with", c.Base)
}
func (j Juice) MakeDrinks() {
	fmt.Println("Squeezes", j.Fruit, "and fills cup with", j.Fruit, "juice")
}
func (ic IceCoffee) MakeDrinks() {
	fmt.Println("Fills cup (", ic.IceLevel, ") with ice, fills cup with", ic.Base)
}

// Unexported
const coffeeShop = "Star Barks"

func Greet() {
	fmt.Println("Welcome to", coffeeShop, "!!")
}
