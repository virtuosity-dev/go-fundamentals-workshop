package main

import "fmt"

type drinks interface {
	makeDrinks()
}
type coffee struct {
	base string
}
type juice struct {
	fruit string
}
type iceCoffee struct {
	coffee
	iceLevel string
}

func (c coffee) makeDrinks() {
	fmt.Println("Fills cup with", c.base)
}
func (j juice) makeDrinks() {
	fmt.Println("Squeezes", j.fruit, "and fills cup with", j.fruit, "juice")
}
func (ic iceCoffee) makeDrinks() {
	fmt.Println("Fills cup (", ic.iceLevel, ") with ice, fills cup with", ic.base)
}

func main() {
	var drinksSlice	[]drinks
	drinksSlice = append(drinksSlice, coffee{base: "Americano"})
	drinksSlice = append(drinksSlice, juice{fruit: "Orange"})
	drinksSlice = append(drinksSlice, iceCoffee{coffee: coffee{base: "Latte"}, iceLevel: "half"})

	for i, d := range drinksSlice {
		fmt.Print(i+1, ") ")
		d.makeDrinks()
	}
}
