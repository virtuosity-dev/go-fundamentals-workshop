package main

import "fmt"

type rect struct {
	color         string
	width, height int
}
// 1, 2a
func (r rect) areaVal() int {
	r.color = "blue"
	return r.width * r.height
}
// 2b
func (r *rect) areaPtr() int {
	r.color = "green"
	return r.width * r.height
}

func main() {
	a := rect{color: "red", width: 10, height: 5}
	b := rect{color: "red", width: 10, height: 5}
	
	// 3
	fmt.Println("3) Area of rect a: ", a.areaVal())
	fmt.Println("3) Area of rect b: ", (&b).areaPtr())
	fmt.Println("3) Color of rect a: ", a.color)
	fmt.Println("3) Color of rect b: ", b.color)
}
