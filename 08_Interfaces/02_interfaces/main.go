package main

import (
	"fmt"
	"math"
)

// 1
type shape interface {
	area() float64
	// a() float64			// if uncommented, rect and circle must have a()
}
type rect struct {
	width, height float64
}
type circle struct {
	radius float64
}

// 2
func (r rect) area() float64 {
	return r.width * r.height
}
func (c circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

// 3
func measure(s shape) {
	fmt.Printf("3)Area of %T %v is: %.2f\n", s, s, s.area())
	// fmt.Printf("3)Area of %s is: %.2f\n", s, s.area())
}
// func (r rect) String() string {
// 	return fmt.Sprintf("Rectangle {width: %.2f, height: %.2f}", r.width, r.height)
// }
// func (c *circle) String() string {
// 	return fmt.Sprintf("Circle {radius: %.2f}", c.radius)
// }

func main() {
	rect1 := rect{width: 10, height: 15}
	circ1 := circle{radius: 10}
	
	// 3
	measure(rect1)
	measure(circ1)
}
