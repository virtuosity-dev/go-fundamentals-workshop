package main

import "fmt"

func main() {
	chnl := make(chan string)
	done := make(chan bool)

	go func() {
		data := []string {"Apple", "Orange", "Watermelon"}
		for _, v := range data {
			fmt.Println("Send: ", v)
			chnl <- v
		}
		close(chnl)
		fmt.Println("Sent all data")
	}()

	go func() {
		// 1
		for v := range chnl {
			fmt.Println("Receive:", v)
		}
		fmt.Println("Receiving stops")
		done <- true
	}()

	<-done
}
