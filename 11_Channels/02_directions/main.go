package main

import "fmt"

// 2
func addTwo(num int, ac chan<- int) {
	sum := num + 2
	ac <- sum
}
func timesFive(ac <-chan int, resc chan<- int) {
	sum := <- ac
	product := sum * 5
	resc <- product
}

func main() {
	// 1
	// able to convert
	c := make(chan int)
	cs := (chan<- int)(c)
	cr := (<-chan int)(c)
	fmt.Printf("1) c:\t%T\n1) cs:\t%T\n1) cr:\t%T\n", c, cs, cr)
	// unable to convert
	// cs2 := make(chan<- int)
	// cr2 := make(<-chan int)
	// c2a = (chan int)(cr)
	// c2b = (chan int)(cs)

	// 2
	num := 3
	addc := make(chan int)
	resultc := make(chan int)

	go addTwo(num, addc)
	go timesFive(addc, resultc)

	fmt.Println("2) Result:", <-resultc)
}
