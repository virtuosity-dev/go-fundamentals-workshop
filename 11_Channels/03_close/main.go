package main

import "fmt"

func main() {
	chnl := make(chan string)
	done := make(chan bool)

	go func() {
		data := []string {"Apple", "Orange", "Watermelon"}
		for _, v := range data {
			fmt.Println("Send: ", v)
			chnl <- v
		}
		// 1
		close(chnl)
		fmt.Println("Sent all data")
	}()

	go func() {
		// 2
		for {
			v, ok := <-chnl
			if ok {
				fmt.Println("Receive:", v)
			} else {
				fmt.Println("Receiving stops")
				done <- true
				return
			}
		}
	}()

	<-done
}
