package main

import (
	"fmt"
	"time"
)

func main() {
	// 1
	unbuff := make(chan int)
	go func() {
		unbuff <- 123
	}()
	fmt.Println("1) Received:", <-unbuff)

	// 2
	done := make(chan bool)
	go func() {
		fmt.Println("2) Start doing stuff")
		time.Sleep(time.Second)
		fmt.Println("2) Finish doing stuff")
		done <- true
	}()
	<- done
	
	// 3
	buff := make(chan string, 2)
	buff <- "Hello"
	buff <- "World"
	
	fmt.Println("3)", <- buff)
	fmt.Println("3)", <- buff)
}
