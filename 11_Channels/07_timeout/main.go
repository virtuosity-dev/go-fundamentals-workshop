package main
import (
    "fmt"
    "time"
)
func main() {
    timeout := make(chan string, 1)
    go func() {
        time.Sleep(2 * time.Second)
        timeout <- "1) Got the results"
    }()
    select {
    case res := <-timeout:
				fmt.Println(res)
		// 1
    case <-time.After(1 * time.Second):
        fmt.Println("1) Operation timeout")
    }

    ontime := make(chan string, 1)
    go func() {
        time.Sleep(2 * time.Second)
        ontime <- "2) Got the results"
    }()
    select {
    case res := <-ontime:
				fmt.Println(res)
		// 1
    case <-time.After(3 * time.Second):
        fmt.Println("2) Operation timeout")
    }
}
