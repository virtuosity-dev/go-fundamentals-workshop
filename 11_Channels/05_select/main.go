package main

import "fmt"

func main() {
	even := make(chan int)
	odd := make(chan int)
	close := make(chan bool)

	go func() {
		// 2
		for i := 1; i <= 10; i++ {
			if i%2 == 0 {
				even <- i
			} else {
				odd <- i
			}
		}
		close <- true
	}()

	for {
		// 4
		fmt.Print("Receive ")
		// 3
		select {
		case v := <-even:
			fmt.Println("Even:", v)
		case v := <-odd:
			fmt.Println("Odd:", v)
		case <-close:
			fmt.Println("Completed")
			return
		}
	}
}
