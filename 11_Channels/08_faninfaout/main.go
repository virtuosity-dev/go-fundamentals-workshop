package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// 1
	data := []int{85, 67, 87, 94, 38, 28, 37, 43, 98, 27}
	datac := generateData(data)

	// 2
	worker1 := expensiveSquare("worker1", datac)
	worker2 := expensiveSquare("worker2", datac)

	// 3
	resultc := fanIn(worker1, worker2)

	for i := 1; i <= len(data); i++ {
		fmt.Printf("Result %v: %v\n", i, <-resultc)
	}
}

func generateData(d []int) <-chan int {
	chnl := make(chan int)
	go func() {
		for _, v := range d {
			chnl <- v
		}
		close(chnl)
	}()
	return chnl
}
// 4
func expensiveSquare(worker string, source <-chan int) <-chan int {
	output := make(chan int)
	go func() {
		for v := range source {
			time.Sleep(time.Duration(5+rand.Intn(5)) * time.Second/5)
			fmt.Println(worker, "processes", v)
			output <- v * v
		}
		close(output)
	}()
	return output
}
// 5
func fanIn(a, b <-chan int) <-chan int {
	output := make(chan int)
	go func() {
		for {
			select {
			case v, ok := <-a:
				if ok {
					output <- v
				}
			case v, ok := <-b:
				if ok {
					output <- v
				}
			}
		}
	}()
	return output
}
