package main

import (
	"fmt"
)

func main() {
	_, err := cusErr(-1)
	if err != nil {
		fmt.Println("3)", err)
	}
}

type customError struct {
	msg, rec, exp string
}
// 3
func (ce *customError) Error() string {
	return fmt.Sprintf("%s. \nReceived: %s, \nExpect: %s", ce.msg, ce.rec, ce.exp)
}
func cusErr(num int) (int, error) {
	if num < 0 {
		err := customError{
			msg: "Invalid parameter",
			rec: fmt.Sprint(num),
			exp: "more than 0",
		}
		return 0, &err
	}
	return 1, nil
}
