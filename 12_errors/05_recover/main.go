package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	doStuff()
	// 4
	fmt.Println("Do other stuff on main()")
}

func doStuff() {
	// 3
	defer func() {
		if r := recover(); r != nil {
			log.Println("Panic recovered... Panic reason:", r)
		}
	}()

	fmt.Println("Do stuff BEFORE panic...")

	_, err := os.Open("no_such.file")
	if err != nil {
		fmt.Println("Panicking!!!")
		// 1
		log.Panicln(err)
	}
	// 2
	fmt.Println("Do stuff AFTER panic...")
}
