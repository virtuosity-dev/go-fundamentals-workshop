package main

import (
	"errors"
	"fmt"
	"os"
)

func main() {
	// 1
	_, err := os.Open("no_such.file")
	if err != nil {
		fmt.Println("1)", err)
	}
	// 2
	_, err = newErr(-1)
	if err != nil {
		fmt.Println("2)", err)
	}
	_, err = fmtErr(-1)
	if err != nil {
		fmt.Println("3)", err)
	}
}

// 2a
func newErr(num int) (int, error) {
	if num < 0 {
		err := errors.New("Invalid parameter, must be greater than 0")
		return 0, err
	}
	return 1, nil
}
// 2b
func fmtErr(num int) (int, error) {
	if num < 0 {
		err := fmt.Errorf("Invalid parameter %v, must be greater than 0", num)
		return 0, err
	}
	return 1, nil
}
