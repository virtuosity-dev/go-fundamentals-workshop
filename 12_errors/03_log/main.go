package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	// 1
	_, err := os.Open("no_such.file")
	if err != nil {
		fmt.Println("1)", err)
	}
	// 2
	_, err = os.Open("no_such.file")
	if err != nil {
		fmt.Print("2) ")
		log.Println(err)
	}
	// 3
	_, err = os.Open("no_such.file")
	if err != nil {
		fmt.Print("3) ")
		log.Fatalln(err)
	}
	// 4
	_, err = os.Open("no_such.file")
	if err != nil {
		fmt.Print("4) ")
		log.Panicln(err)
	}
}
