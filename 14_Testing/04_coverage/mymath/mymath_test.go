package mymath

import (
	"fmt"
	"testing"
)

func TestSumOf(t *testing.T) {
	type tester struct {
		data     []int
		expected int
	}

	testers := []tester{
		tester{[]int{1, 2}, 3},
		tester{[]int{1, 2, 3, 4, 5}, 15},
		tester{[]int{1, 0, 1, 0, 0}, 2},
		tester{[]int{5, -1, -2, -3}, -1},
	}

	for _, v := range testers {
		got := SumOf(v.data...)
		if got != v.expected {
			t.Errorf("\nTest SumOf(): %v\nExpected: %v\nGot: %v", v.data, v.expected, got)
		}
	}
}

func ExampleSumOf() {
	fmt.Println(SumOf(1, 2, 3))
	// Output:
	// 6
}
