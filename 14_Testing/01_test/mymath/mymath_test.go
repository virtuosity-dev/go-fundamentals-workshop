// 1a
package mymath

import "testing"

// 1b
func TestSumOf(t *testing.T) {
	got := SumOf(1, 2)
	if(got != 3) {
		// 1c
		t.Errorf("\nTest SumOf(): %v\nExpected: %v\nGot: %v", "[1,2]", 3, got)
	}
}
