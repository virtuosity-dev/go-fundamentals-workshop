package main

import (
	"fmt"
	. "gitlab.com/virtuosity-dev/go-fundamentals-workshop/14_Testing/01_test/mymath"
)

func main() {
	ints := []int{1,2,3,4,5}
	fmt.Println("Sum of", ints, "is", SumOf(ints...))
	fmt.Println("Sum of [1, 2] is", SumOf(1,2))
}
