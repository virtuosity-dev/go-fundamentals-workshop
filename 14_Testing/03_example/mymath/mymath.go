// Package mymath provides functions for arithmetic calculations
package mymath

// SumOf gives the total sum of all given integers
func SumOf(a ...int) int {
	sum := 0
	for _, v := range a {
		sum += v
	}
	return sum
}
