package mymath

import "fmt"

func ExampleSumOf() {
	fmt.Println(SumOf(1,2,3))
	// Output:
	// 6
}
