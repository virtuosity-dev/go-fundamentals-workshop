package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "An eye for eye only ends up making the whole world blind. Happiness is when what you think, what you say, and what you do are in harmony. Where there is love there is life. - Mahatma Gandhi"
	splitStr := strings.Split(str, " ")
	fmt.Println(concat(splitStr))
	fmt.Println(join(splitStr))
}
// 1
func concat(slc []string) string {
	s := ""
	for _, v := range slc {
		s += v
		s += " "
	}
	return s
}
func join(slc []string) string {
	return strings.Join(slc, " ")
}
