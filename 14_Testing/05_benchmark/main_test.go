package main

import (
	"strings"
	"testing"
)

const str = "An eye for eye only ends up making the whole world blind. Happiness is when what you think, what you say, and what you do are in harmony. Where there is love there is life. - Mahatma Gandhi"
// 2
func BenchmarkConcat(b *testing.B) {
	slc := strings.Split(str, " ")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		concat(slc)
	}
}
func BenchmarkJoin(b *testing.B) {
	slc := strings.Split(str, " ")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		join(slc)
	}
}
