package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Fullname string `json:"Fname"`
	Age      int
	Cart     []string
	secret   bool
}

func main() {
	s := `[{"Fname":"James Bond","Age":30,"Cart":["Shades","Suit","Boots","Gun"]},
	{"Fname":"Darth Vader","Age":45,"Cart":["Helmet","Armor","Cape","Lightsaber"]}]`
	// 1
	bs := []byte(s)
	fmt.Printf("1) %T\n", s)
	fmt.Printf("1) %T\n", bs)

	// 2
	var users []User

	// 3
	err := json.Unmarshal(bs, &users)
	if err != nil {
		fmt.Println(err)
	}

	for i, u := range users {
		fmt.Print("3.", i+1, ") ")
		fmt.Println(u.Fullname, u.Age, u.Cart, u.secret)
	}
}
