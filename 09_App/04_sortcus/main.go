package main

import (
	"fmt"
	"sort"
)

// 1
type byLength []string

// 2
func (s byLength) Len() int {
	return len(s)
}
func (s byLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byLength) Less(i, j int) bool {
	return len(s[i]) < len(s[j])
}

func main() {
	strs := []string{"Mango", "Durian", "Jackfruit"}
	// 3
	sort.Sort(byLength(strs))
	fmt.Println("1) Sorted string slice:", strs)
}
