package main

import (
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

func main() {
	// 1
	password := `password123`
	encrypted, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("1) Original password:", password)
	fmt.Println("1) Encrypted password:", encrypted)

	// 2
	attempt := `password1234`
	err = bcrypt.CompareHashAndPassword(encrypted, []byte(attempt))
	if err != nil {
		fmt.Println("2) Invalid password!")
		return
	}
	fmt.Println("2) You're logged in")
}
