package main

import (
	"fmt"
	"sort"
)

func main() {
	// 1
	strs := []string{"Mango", "Durian", "Jackfruit"}
	sort.Strings(strs)
	fmt.Println("1) Sorted string slice:", strs)
	
	ints := []int{2048, 1024, 4096}
	sort.Ints(ints)
	fmt.Println("1) Sorted int slice:", ints)
	
	// 2
	fmt.Println("2) Is sorted?:", sort.IntsAreSorted(ints))
}
