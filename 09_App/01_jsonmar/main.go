package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Fullname string		`json:"Fname"`
	Age      int
	Cart     []string
	secret   bool
}

func main() {
	// 1
	jsonBool, _ := json.Marshal(true)
	fmt.Println("1a) Boolean:", string(jsonBool))
	jsonInt, _ := json.Marshal(73)
	fmt.Println("1b) Int:", string(jsonInt))
	jsonFlt, _ := json.Marshal(3.14159)
	fmt.Println("1c) Float:", string(jsonFlt))
	jsonStr, _ := json.Marshal("Hello, world!")
	fmt.Println("1d) String:", string(jsonStr))
	jsonSlc, _ := json.Marshal([]interface{}{1, 1, 2, 3, 5, 8, 13, 21, "你好👋", true, 2.71828})
	fmt.Println("1e) Slice:", string(jsonSlc))
	jsonMap, _ := json.Marshal(map[string]int{"one": 1, "two": 2, "three": 3})
	fmt.Println("1f) Map:", string(jsonMap))
	
	// 2
	jsonStruct, _ := json.Marshal(User{
		Fullname: "James Bond",
		Age:      30,
		Cart:     []string{"Shades", "Suit", "Boots", "Gun"},
		secret:   false,
	})
	fmt.Println("2) Struct:", string(jsonStruct))
}
