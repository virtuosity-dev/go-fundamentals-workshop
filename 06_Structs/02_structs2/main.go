package main

import "fmt"

// 1
type coffee struct {
	name  string
	price float32
}

// 2
type promotion struct {
	coffee
	price float32
}

func main() {
	// 2
	promo := promotion{
		coffee: coffee{
			name:  "Flat White",
			price: 3.80,
		},
		price: 2.80,
	}
	fmt.Println("2a)", promo.name, promo.coffee.price, promo.price)

	// 3
	person := struct {
		name string
		age  int
	}{
		name: "Kim",
		age:  22,
	}
	fmt.Println("3) Anonymous struct:", person)
}
