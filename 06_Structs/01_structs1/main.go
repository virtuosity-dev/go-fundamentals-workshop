package main

import "fmt"

// 1
type coffee struct {
	name  string
	price float32
}

func main() {
	// 1
	coffee1 := coffee{
		name:  "Long Black",
		price: 3.20,
	}
	fmt.Println("1a)", coffee1)
	fmt.Println("1b)", coffee1.name, coffee1.price)

	coffee1.price = 3.50
	fmt.Println("1c)", coffee1)
}
