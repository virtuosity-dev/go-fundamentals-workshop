/*
	Package drinks provides structs and methods for making drinks.

	Sentences with a fullstop is a paragraph, not a Heading.
	
	This is a Heading, one line, same indentation, no fullstops

	Paragraphs
	need
	to be seperated
	by
	new line.

		We create a block with indentation.
*/
package drinks

import "fmt"

// Drinks interface is implemented by any value that has the MakeDrinks method.
// The MakeDrinks method is used to, make drinks.
type Drinks interface {
	MakeDrinks()
}
// Coffee can be use to create hot coffee
type Coffee struct {
	// Base sets the type of the coffee
	Base string
}
// Juice can be use to create fruit juices
type Juice struct {
	// Fruit sets the type of the juice
	Fruit string
}
// IceCoffee can be used to create ice coffee
type IceCoffee struct {
	// Coffee is used to create the Coffee
	Coffee
	// IceLevel puts ice into Coffee
	IceLevel string
}

// MakeDrinks makes coffee with Coffee's Base value
func (c Coffee) MakeDrinks() {
	fmt.Println("Fills cup with", c.Base)
}
// MakeDrinks makes fruit juice with Juice's Fruit value
func (j Juice) MakeDrinks() {
	fmt.Println("Squeezes", j.Fruit, "and fills cup with", j.Fruit, "juice")
}
// MakeDrinks makes ice coffee with IceCoffee's Coffee and IceLevel values
func (ic IceCoffee) MakeDrinks() {
	fmt.Println("Fills cup (", ic.IceLevel, ") with ice, fills cup with", ic.Base)
}

const coffeeShop = "Star Barks"

// Greet greets the customers
func Greet() {
	fmt.Println("Welcome to", coffeeShop, "!!")
}
