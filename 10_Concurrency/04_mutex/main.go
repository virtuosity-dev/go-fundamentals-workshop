package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	counter := 0
	final := 100

	var wg sync.WaitGroup
	wg.Add(final)
	// 1
	var me sync.Mutex
	
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	for i := 0; i < final; i++ {
		go func() {
			defer wg.Done()
			// 2
			me.Lock()
			c := counter
			runtime.Gosched()
			counter = c + 1
			// 3
			me.Unlock()
		}()
		fmt.Println("Goroutines:", runtime.NumGoroutine())
	}

	wg.Wait()
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	fmt.Println("Counter:", counter)
}
