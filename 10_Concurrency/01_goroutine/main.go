package main

import (
	"fmt"
	"time"
	"gitlab.com/virtuosity-dev/go-fundamentals-workshop/10_Concurrency/01_goroutine/action"
)

// 1
func prepareForWork() {
	action.Eat()
	action.Drink()
	action.Pack()
}
// 2
func lateForWork() {
	go action.Pack()
	action.Eat()
	action.Drink()
}
// 3
func reallyLateForWork() {
	go action.Eat()
	go action.Drink()
	go action.Pack()
}

func main() {
	startTime := time.Now()
	// prepareForWork()
	// lateForWork()
	reallyLateForWork()
	endTime := time.Now()
	fmt.Printf("Leave to work. Time passed: %.2f seconds", endTime.Sub(startTime).Seconds())
}
