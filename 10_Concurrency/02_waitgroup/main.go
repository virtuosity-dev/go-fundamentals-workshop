package main

import (
	"fmt"
	"sync"
	"time"
	"gitlab.com/virtuosity-dev/go-fundamentals-workshop/10_Concurrency/02_waitgroup/action"
)

func reallyLateForWork() {
	// 1
	var wg sync.WaitGroup
	// 2
	wg.Add(3)

	go action.Eat(&wg)
	go action.Drink(&wg)
	go action.Pack(&wg)

	// 3
	wg.Wait()
}

func main() {
	startTime := time.Now()
	reallyLateForWork()
	endTime := time.Now()
	fmt.Printf("Leave to work. Time passed: %.2f seconds", endTime.Sub(startTime).Seconds())
}
