package action

import (
	"fmt"
	"sync"
	"time"
)

const food string = "sandwich"
const drink string = "coffee"
const taskTime time.Duration = time.Second/2

func Eat(wg *sync.WaitGroup) {
	// 4
	defer wg.Done()
	time.Sleep(taskTime)
	fmt.Println("Take 1st bite of", food)
	time.Sleep(taskTime)
	fmt.Println("Take 2nd bite of", food)
	time.Sleep(taskTime)
	fmt.Println("Shallow the rest of the", food)
}

func Drink(wg *sync.WaitGroup) {
	// 4
	defer wg.Done()
	time.Sleep(taskTime)
	fmt.Println("Take 1st sip of", drink)
	time.Sleep(taskTime)
	fmt.Println("Take 2nd sip of", drink)
	time.Sleep(taskTime)
	fmt.Println("Gulp down the rest of the", drink)
}

func Pack(wg *sync.WaitGroup) {
	// 4
	defer wg.Done()
	time.Sleep(taskTime)
	fmt.Println("Pack laptop")
	time.Sleep(taskTime)
	fmt.Println("Pack phone")
	time.Sleep(taskTime)
	fmt.Println("Pack charger")
}