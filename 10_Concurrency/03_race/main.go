package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	counter := 0
	final := 100

	var wg sync.WaitGroup
	wg.Add(final)
	
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	// 1
	for i := 0; i < final; i++ {
		go func() {
			defer wg.Done()
			c := counter
			// 2
			runtime.Gosched()
			counter = c + 1
		}()
		// 3
		fmt.Println("Goroutines:", runtime.NumGoroutine())
	}

	wg.Wait()
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	fmt.Println("Counter:", counter)
}
