package main

import (
	"fmt"
)

// 5
type Direction int

const (
	North Direction = iota
	South
	East
	West
)

func (d Direction) String() string {
	return [...]string{"NORTH", "SOUTH", "EAST", "WEST"}[d]
}

func main() {

	// 5
	var dir Direction = North
	fmt.Print("5) ", dir)
	switch dir {
	case North:
	    fmt.Println(" goes up.")
	case South:
	    fmt.Println(" goes down.")
	case East:
	    fmt.Println(" goes right.")
	case West:
	    fmt.Println(" goes left.")
	default:
	    fmt.Println(" stay.")
	}
}
