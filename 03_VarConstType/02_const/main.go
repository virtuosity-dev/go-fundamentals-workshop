package main

import (
	"fmt"
	"math"
)

// 2
const out = "outside"

func main() {
	// 1
	const iam = "i am a constant"
	// iam = "once declared, i cant be changed"
	fmt.Println("1)", iam)

	// 2
	const in = "inside"
	fmt.Println("2) const can be declare", out, "and", in, "of a function")

	// 3
	// const empty
	// const computed = math.Sqrt(4)
	fmt.Println("3) Uncommenting any of these will cause error")
	
	// 4
	fmt.Printf("4) %.50f\n", math.Sqrt(math.Pi))
	
	// 5
	const a int = 100
	const b = int64(100)
	const cConst = 100
	var c = cConst
	fmt.Printf("5) %T, %T, %T, %T\n", a, b, c, math.Sin(cConst))
}
