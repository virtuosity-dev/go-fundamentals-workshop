package main

import (
	"fmt"
)

// 6
var outsidevar = "I am declared outside of the function"
//outsideshort := "Me too"					// uncommenting this line will cause an error

func main() {
	// 1
	var itemno = "1)"
	var name, age = "Kim", 22
	fmt.Println(itemno, name, "is", age, "years old.")
	
	// 2
	age = 40
	fmt.Println("2)", name, "is now", age, "years old.")

	// 3
	var foo = 1
	var bar int = 1
	fmt.Println("3)", foo, "is the same as", bar)
	
	// 4
	var zerostr string
	var zeroint int
	fmt.Println("4) We are both zero-valued,", zerostr, ",", zeroint)
	
	// 5
	a, b := "alpha", "beta"
	fmt.Println("5)", a, "and", b, "short declaration")
	
	// 6
	fmt.Println("6)", outsidevar)
}