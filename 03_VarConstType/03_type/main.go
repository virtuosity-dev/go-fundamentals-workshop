package main

import (
	"fmt"
	"math"
)

// 4
type idnumber int

func main() {
	// 1
	var hello = "Hello, " + "World!"
	fmt.Printf("1) %T: %v\n", hello, hello)

	// 2
	var three = 1 + 2
	var halfPi = math.Pi / 2.0
	fmt.Printf("2a) %T: %v\n", three, three)
	fmt.Printf("2b) %T: %v\n", halfPi, halfPi)
	
	// 3
	fmt.Println("3a)", true && false)
	fmt.Println("3b)", true || false)
	fmt.Println("3c)", !true)
	
	// 4
	var id idnumber = 10
	fmt.Printf("4) %T: %v\n", id, id)
	
	// 5
	// var idIntUc int = id		// this will cause error
	var idInt int = int(id)
	fmt.Printf("5) %T: %v\n", idInt, idInt)
}
