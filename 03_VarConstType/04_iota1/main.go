package main

import (
	"fmt"
)

func main() {
	// 1
	const (
		a = iota //iota = 0
		b = iota //iota = 1
		c = 9999 //iota = 2 unused
		d = iota //iota = 3
	)
	fmt.Println("1)", a, b, c, d)
	
	// 2
	const i = iota
	const j = iota
	fmt.Println("2)", i, j)

	// 3
	const (
		p = iota
		q
		r
		s
	)
	fmt.Println("3)", p, q, r, s)

	// 4
	const (
		x = (iota + 1) * 2
		y
		_
		z
	)
	fmt.Println("4)", x, y, z)
}
