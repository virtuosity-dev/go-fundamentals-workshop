package main

import (
	"fmt"
)

func main() {
	// 1
	a := 0
	fmt.Printf("1) a's value: %v, a's type: %T\n", a, a)
	fmt.Printf("1) &a's value: %v, &a's type: %T\n", &a, &a)
	
	// 2
	b := &a
	fmt.Printf("2) b's value: %v, b's type: %T\n", b, b)
	fmt.Printf("2) *b's value: %v, *b's type: %T\n", *b, *b)
	
	// 3
	*b = 123
	fmt.Printf("3) a's value: %v\n", a)

	// 4
	var np *int
	fmt.Printf("2) np's value: %v, np's type: %T\n", np, np)
	fmt.Printf("2) *np's value: %v, *np's type: %T\n", *np, *np)
}
