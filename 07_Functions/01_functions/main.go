package main

import (
	"fmt"
	"strconv"
)

// 1
func plusConcat(a, b, c int, s string) string {
	return strconv.Itoa(a+b+c) + s
}
// 2
func sumProd(a, b int) (int, int) {
	return a + b, a * b
}
// 3
func sumAll(name string, nums ...int) (string, int) {
	total := 0
	for _, v := range nums {
		total += v
	}
	return name + ":", total
}

func main() {
	// 1
	fmt.Println("1)", plusConcat(1, 2, 4, "Eleven"))
	
	// 2
	sum, product := sumProd(3, 4)
	fmt.Println("2) Sum:", sum, "; Product:", product)
	
	// 3
	name, total := sumAll("Sum", 1, 2, 3, 4, 5)
	fmt.Println("3)", name, total)
	
	sumArray := []int{2, 4, 6, 8, 10}
	n, t := sumAll("Sum Arr", sumArray...)
	fmt.Println("3)", n, t)
}
