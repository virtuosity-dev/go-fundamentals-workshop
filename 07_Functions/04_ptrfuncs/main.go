package main

import "fmt"

// 1
func squareThis(num *int) {
	*num = *num * *num
	fmt.Println("1) Number inside 'squareThis()':", *num)
}
func squareTest(num int) {
	num = num * num
	fmt.Println("-) Number inside 'squareTest()':", num)
}
// 2
func squareOf(num int) int {
	num = num * num
	fmt.Println("2) Number inside 'squareOf()':", num)
	return num
}

func main() {
	// 1
	number := 8
	fmt.Println("1) Number before 'squareThis()':", number)
	squareThis(&number)
	fmt.Println("1) Number after 'squareThis()':", number)
	fmt.Println("")

	number = 8
	fmt.Println("-) Number before 'squareTest()':", number)
	squareTest(number)
	fmt.Println("-) Number after 'squareTest()':", number)
	fmt.Println("")
	
	// 2
	number = 8
	fmt.Println("2) Number before 'squareOf()':", number)
	newNumber := squareOf(number)
	fmt.Println("2) Number after 'squareOf()':", number)
	fmt.Println("2) New number after 'squareOf()':", newNumber)
}
