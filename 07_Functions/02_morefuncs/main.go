package main

import "fmt"

// 1
func count(from int) func() int {
	num := from
	return func() int {
		num++
		return num
	}
}
// 2
func sumTo(n int) int {
	if(n <= 1) {
		return 1
	}
	return n + sumTo(n-1)
}
// 3
func printOneTwo() {
	defer fmt.Print("One ")
	fmt.Print("Two ")
}

func main() {
	// 1
	nextCount := count(0)
	fmt.Println("1) Count:", nextCount(), nextCount(), nextCount())
	newCount := count(0)
	fmt.Println("1) New count:", newCount(), newCount())
	
	// 2
	fmt.Println("2) Sum to 10:", sumTo(10))

	// 3
	fmt.Print("3) ")
	printOneTwo()
	fmt.Println("Three ")
}
