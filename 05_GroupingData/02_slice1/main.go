package main

import (
	"fmt"
)

func main() {
	// 1
	var zeroedSlice []int
	mySlice := make([]int, 3) 
	fmt.Println("1) Zero-valued slice:", zeroedSlice)
	fmt.Println("1) Slice with empty values:", mySlice)
	
	// 2
	// zeroedSlice[2] = 128			// index out of range error
	mySlice[2] = 128
	fmt.Println("2) mySlice:", mySlice)
	fmt.Println("2) index [2] of mySlice has value:", mySlice[2])
	
	// 3
	fmt.Println("3) Length of mySlice:", len(mySlice))
	
	// 4
	numberSlice := []string{"zero", "one", "two", "three"}
	fmt.Println("4) One-liner Number Slice:", numberSlice)
	
	// 5
	twoDSlice := [][]int{
		[]int{0, 1, 2}, 
		[]int{3, 5}, 
		[]int{2, 4, 8, 16, 32}}
	fmt.Println("5) 2-d slice:", twoDSlice)

	// 6
	for i, v := range numberSlice {
		fmt.Printf("6) %v -> %s\n", i, v)
	}
}
