package main

import (
	"fmt"
)

func main() {
	// 1
	ageOf := make(map[string]int)
	fmt.Println("1) Empty map:", ageOf)
	
	// 2
	ageOf["Kim"] = 22
	ageOf["Jedi"] = 35
	ageOf["BonJovi"] = 60
	fmt.Println("2) Age map: ", ageOf)
	
	// 3
	kimsAge := ageOf["Kim"]
	fmt.Println("3) Kim is", kimsAge, "years old")
	_, yodaExists := ageOf["Yoda"]
	fmt.Println("3) Is Yoda in the map?", yodaExists)
	
	// 4
	delete(ageOf, "Jedi")
	fmt.Println("4) Age map: ", ageOf)
	
	// 5
	fmt.Println("5) Length of age map: ", len(ageOf))
	
	// 6
	sounds := map[string]string{"Cat": "meow", "Bird": "tweet", "Cow": "Moo"}
	fmt.Println("6) Sounds map:", sounds)

	// 7
	for k, v := range sounds {
		fmt.Printf("7) %s goes %s\n", k, v)
	}
}
