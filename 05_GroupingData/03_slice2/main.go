package main

import (
	"fmt"
)

func main() {
	// 1
	var mySlice []int
	mySlice = append(mySlice, 0)
	mySlice = append(mySlice, 1, 2, 3, 4, 5)
	fmt.Println("1) mySlice:", mySlice)
	
	// 2
	copySlice := make([]int, len(mySlice))
	copy(copySlice, mySlice)
	fmt.Println("2) copySlice:", copySlice)
	shorterSlice := make([]int, 3)
	longerSlice := make([]int, 8)
	copy(shorterSlice, mySlice)
	copy(longerSlice, mySlice)
	fmt.Println("2) short:", shorterSlice, ", long:", longerSlice)
	
	// 3
	a := mySlice[1:4]
	b := mySlice[:5]
	c := mySlice[2:]
	fmt.Println("3) Slice operations: a)", a, ",b)", b, ",c)", c)
}
