package main

import (
	"fmt"
)

func main() {
	// 1
	var arr [5]int
	fmt.Println("1) arr:", arr)
	
	// 2
	arr[2] = 128
	fmt.Println("2) arr:", arr)
	fmt.Println("2) index [2] of arr has value:", arr[2])
	
	// 3
	fmt.Println("3) Length of arr:", len(arr))
	
	// 4
	newArr := [5]int{1, 2, 3, 4, 5}
	fmt.Println("4) One-liner newArr:", newArr)
	
	// 5
	ticTacToe := [3][3]string{
		[3]string{"X", "O", "O"}, 
		[3]string{"X", "X", " "}, 
		[3]string{"X", " ", "O"}}
	fmt.Println("5) 2d array:", ticTacToe)
	fmt.Println("")
	fmt.Println("5) ticTacToe:", ticTacToe[0])
	fmt.Println("5) ticTacToe:", ticTacToe[1])
	fmt.Println("5) ticTacToe:", ticTacToe[2])
}
