package main

import (
	"fmt"
)

func main() {
	// 1
	name, gender := "John", "male"
	switch gender {
	case "male":
		fmt.Println("1) His name is", name)
	case "female":
		fmt.Println("1) Her name is", name)
	default:
		fmt.Println("1) Hello,", name)
	}

	// 2
	dice := 5
	switch dice {
	case 1, 2, 3:
		fmt.Println("2)", dice, "is small")
	case 4, 5, 6:
		fmt.Println("2)", dice, "is big")
	default:
		fmt.Println("2)", dice, "is not valid")
	}
}
