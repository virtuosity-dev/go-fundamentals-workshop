package main

import (
	"fmt"
)

func main() {
	// 1
	a := 9
	if a%2 == 0 {
		fmt.Println("1)", a, "is an even number")
	} else {
		fmt.Println("1)", a, "is an odd number")
	}
	
	// 2
	earth := "round"
	fmt.Println("2) The earth is round")
	if (earth == "flat") {
		fmt.Println("2) No, the earth is flat")
	}
	
	// 3
	if num := 20; num < 0 {
		fmt.Println("3)", num, "is a negative number")
	} else if num < 10 {
		fmt.Println("3)", num, "is a single digit number")
	} else {
		fmt.Println("3)", num, "has more than 1 digit")
	}
	// the scope of "num" is limited within the if statement
	// fmt.Println(num)
}
