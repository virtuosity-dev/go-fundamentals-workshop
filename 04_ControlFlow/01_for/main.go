package main

import (
	"fmt"
	"math"
)

func main() {
	// 1
	fmt.Print("1) I can count to ten: ")
	a := 1
	for a <= 10 {
		fmt.Print(a, " ")
		a++
	}

	// 2
	fmt.Print("\n2) The 9 times table: ")
	for b := 1; b <= 12; b++ {
		fmt.Print(b*9, " ")
	}

	// 3
	fmt.Print("\n3) Power of 2 less than 100: ")
	n := 0
	for {
		twoPowN := math.Pow(2, float64(n))
		if twoPowN > 100 {
			break
		}
		fmt.Print(twoPowN, " ")
		n++
	}
	
	// 4
	fmt.Print("\n4) Odd numbers: ")
	for n := 0; n <= 10; n++ {
		if n%2 == 0 {
			continue
		}
		fmt.Print(n, " ")
	}
}
