package main

import (
	"fmt"
	"time"
)

func main() {
	// 3
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("3) Good morning!")
	case t.Hour() >= 12, t.Hour() < 17:
		fmt.Println("3) Good afternoon!")
	case t.Hour() >= 17:
		fmt.Println("3) Good evening!")
	}
	
	// 4
	printType := func(i interface{}) {
		switch t := i.(type) {
		case bool:
			fmt.Println("4)", i, "is a bool")
		case int:
			fmt.Println("4)", i, "is an int")
		default:
 			fmt.Printf("4) %v is of unknown type %T\n", i, t)
		}
	}
	printType(true)
	printType(123)
	printType("Hello, World")
}
